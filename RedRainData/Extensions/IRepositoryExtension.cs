﻿using Microsoft.EntityFrameworkCore;
using RedRainData.Repository;

namespace RedRainData.Extensions
{
    public static class IRepositoryExtension
    {
        public static IRepository Default(this IRepository repository, string connectionString)
        {
            var options = new DbContextOptionsBuilder<EntityFrameworkRepository>()
                .UseSqlServer(connectionString)
                .Options;

            return new EntityFrameworkRepository(options);            
        }
    }
}
