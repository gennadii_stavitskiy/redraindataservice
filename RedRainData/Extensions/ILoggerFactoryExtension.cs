﻿using Microsoft.Extensions.Logging;

namespace RedRainData.Extensions
{
    public static class ILoggerFactoryExtension
    {
        public static ILoggerFactory Default(this ILoggerFactory factory)
        {
            return new LoggerFactory();            
        }
    }
}
