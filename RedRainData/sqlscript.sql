﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.Entities_List'))
   exec('
CREATE PROCEDURE [dbo].[Entities_List] 
 @Type NVARCHAR(50)
AS 
BEGIN 
SET NOCOUNT ON; 

SELECT * FROM Entities WHERE [Type] LIKE @Type

END')
GO

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Entities'))
BEGIN
    

CREATE TABLE [dbo].[Entities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Content] NVARCHAR(max) NOT NULL,
	[Created] DATETIME2 NOT NULL DEFAULT GETDATE(),
	[Type] NVARCHAR(50) NOT NULL,
 CONSTRAINT [PK_Entities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


INSERT INTO [dbo].[Entities]
           ([Content]           
           ,[Type])
     VALUES
           ('Any Content1','T1'),
		   ('Any Content2','T1'),
		   ('Any Content3','T1'),
		   ('Any Content4','T1'),
		   ('Any Content5','T1'),
		   ('Any Content6','T2'),
		   ('Any Content7','T2'),
		   ('Any Content8','T2'),
		   ('Any Content9','T2'),
		   ('Any Content10','T2'),
		   ('Any Content11','T3'),
		   ('Any Content12','T3'),
		   ('Any Content13','T3'),
		   ('Any Content14','T3'),
		   ('Any Content15','T3')


END
GO
