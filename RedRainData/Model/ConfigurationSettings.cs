﻿using Microsoft.Extensions.Configuration;
using RedRainData.Util;
using System.IO;

namespace RedRainData.Model
{
    /// <summary>
    /// Application configuration settings
    /// </summary>
    public class ConfigurationSettings
    {
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets the default configuration from local configuration.json.
        /// </summary>
        /// <returns></returns>
        public static ConfigurationSettings Default()
        {
            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("configuration.json");

            var configuration = builder.Build();

            var connectionString = configuration["ConnectionString:TestDatabase"];

            Guard.NotNullOrEmpty(connectionString, "connectionString");

            return new ConfigurationSettings { ConnectionString = connectionString };
        }
    }
}
