﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RedRainData.Model
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }

        public DateTime Created { get; set; } = DateTime.Now;

        [Required]
        [MaxLength(50)]
        public string Type { get; set; }

        [Required]
        public string Content { get; set; }
    }
}
