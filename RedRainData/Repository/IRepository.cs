﻿using RedRainData.Model;
using System;
using System.Collections.Generic;

namespace RedRainData.Repository
{
    /// <summary>
    /// IRepository interface
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IRepository: IDisposable
    {
        /// <summary>
        /// Gets the type of the by type parameter.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>List of Entity objects</returns>
        List<Entity> GetByType(string type);

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Initialize();
    }
}
