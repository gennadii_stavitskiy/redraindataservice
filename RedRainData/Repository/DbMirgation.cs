﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using RedRainData.Repository;

namespace RedRainData
{
    /// <summary>
    /// DbMirgation encapsulate logic to initialize DB firs time.
    /// </summary>
    /// <seealso cref="Microsoft.EntityFrameworkCore.Migrations.Migration" />
    [DbContext(typeof(EntityFrameworkRepository))]
    [Migration("CustomMigration_Create_Entities_List")]
    public partial class DbMirgation: Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = Resource1.sqlscript;

            migrationBuilder.Sql(sql);
        }        
    }
}
