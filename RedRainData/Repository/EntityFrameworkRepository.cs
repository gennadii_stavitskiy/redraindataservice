﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using RedRainData.Model;
using RedRainData.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace RedRainData.Repository
{
    /// <summary>
    /// EntityFrameworkRepository represent repository which encapsulates Entity Framework Core on behind
    /// </summary>
    /// <seealso cref="Microsoft.EntityFrameworkCore.DbContext" />
    /// <seealso cref="RedRainData.Repository.IRepository" />
    public class EntityFrameworkRepository : DbContext, IRepository
    {
        private static readonly Object _lockObj = new Object();

        private static bool _isInitialized;        

        public EntityFrameworkRepository(DbContextOptions<EntityFrameworkRepository> options) : base(options)
        {
           
        }

        /// <summary>
        /// Initializes this instance and performs initial check if db exists and generateds data and schema definitions.
        /// </summary>
        public void Initialize()
        {
            if (!_isInitialized)
            {
                lock (_lockObj)
                {
                    if (!_isInitialized)
                    {
                        //create table and SPs if not exists
                        //logic located in DbMigration 
                        Database.Migrate();

                        _isInitialized = true;
                    }
                }                
            }
        }

        /// <summary>
        /// Gets or sets the entities.
        /// </summary>
        /// <value>
        /// The entities.
        /// </value>
        public DbSet<Entity> Entities { get; set; }

        /// <summary>
        /// Gets the type of the by type parameter.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// List of Entity objects
        /// </returns>
        public List<Entity> GetByType(string type)
        {
            Guard.NotNullOrEmpty(type, nameof(type));

            var typeParam = new SqlParameter("Type", type);

            return Entities
                .FromSql("EXECUTE dbo.Entities_List @Type", typeParam)
                .ToList();
        }        
    }
}
