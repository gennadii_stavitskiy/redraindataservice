﻿using System;
using System.Collections.Generic;
using RedRainData.Model;

namespace RedRainData
{
    /// <summary>
    /// IDataService interface represents methods to fetch data from Db
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IDataService: IDisposable
    {
        List<Entity> GetEntities(string type);
    }
}