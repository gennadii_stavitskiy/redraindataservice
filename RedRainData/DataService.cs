﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RedRainData.Extensions;
using RedRainData.Model;
using RedRainData.Repository;
using RedRainData.Util;
using System.Collections.Generic;

namespace RedRainData
{
    /// <summary>
    /// DataService class represents service which allow to fetch data from Db
    /// </summary>
    /// <seealso cref="RedRainData.IDataService" />
    public class DataService : IDataService
    {
        IRepository _repository;

        ILogger _logger;

        public DataService(IOptions<ConfigurationSettings> configSettingsOptions = null, IRepository repository = null, ILoggerFactory loggerFactory = null)
        {
            _logger = (loggerFactory ?? loggerFactory.Default()).CreateLogger<DataService>();
            
            var connectionString = configSettingsOptions != null ? configSettingsOptions.Value.ConnectionString : ConfigurationSettings.Default().ConnectionString;
            
            _repository = repository ?? repository.Default(connectionString);            

            //initialize db schema/data
            _repository.Initialize();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if(_repository != null)
            {
                _repository.Dispose();
            }
        }

        /// <summary>
        /// Gets the entities filtered by type parameter.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>List of Entity objects</returns>
        public List<Entity> GetEntities(string type)
        {
            try
            {
                Guard.NotNullOrEmpty(type, nameof(type));

                return _repository.GetByType(type);
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "DataService.GetEntities");
                throw;
            }
            
        }
    }
}
