using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RedRainData.Model;
using RedRainData.Repository;
using System;

namespace RedRainData.Tests
{
    [TestClass]
    public class DataServiceTests
    {
        IOptions<ConfigurationSettings> _configSettingsOptions;

        [TestInitialize]
        public void TestInitialize()
        {
            _configSettingsOptions = Options.Create(new ConfigurationSettings { ConnectionString = "ConnectionString" });           
        }

        [TestMethod]
        public void DataService_CanCreate()
        {
            using (var service = new DataService()) { };            
        }

        [TestMethod]
        public void DataService_GetEntities_CallRepositoryGetEntities()
        {
            var repository = Mock.Of<IRepository>();
            var service = new DataService(_configSettingsOptions, repository);
                        
            var type = "Type1";

            var entities = service.GetEntities(type);

            Mock.Get(repository).Verify(r => r.GetByType(type));
        }

        [TestMethod]
        public void DataService_Dispose_CallRepositoryDispose()
        {
            var repository = Mock.Of<IRepository>();

            using (var service = new DataService(_configSettingsOptions, repository)) { };
            
            Mock.Get(repository).Verify(r => r.Dispose());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DataService_GetEntities_ThrowArgumentNullExceptionIfParameterIsNull()
        {
            var service = new DataService();

            service.GetEntities(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DataService_GetEntities_RepositoryException_LoggerLogCritical()
        {
            var repository = Mock.Of<IRepository>();

            var logger = Mock.Of<ILogger>();

            var loggerFactory = Mock.Of<ILoggerFactory>();

            Mock.Get(loggerFactory).Setup(l => l.CreateLogger(It.IsAny<string>())).Returns(logger);

            Mock.Get(repository).Setup(r=> r.GetByType("T1")).Throws(new ArgumentNullException());

            var service = new DataService(null, repository);

            service.GetEntities("T1");

            Mock.Get(logger).Verify(l => l.LogCritical(It.IsAny<System.ArgumentNullException>(), It.IsAny<string>()));
        }

    }
}
