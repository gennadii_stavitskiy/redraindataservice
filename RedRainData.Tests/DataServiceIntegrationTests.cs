using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RedRainData.Model;
using RedRainData.Repository;
using System.Linq;

namespace RedRainData.Tests
{
    [TestClass]
    public class DataServiceIntegrationTests
    {
        [TestMethod]
        public void DataService_Integration_GetEntities_ReturnCorrectEntrie()
        {
            var config = ConfigurationSettings.Default();

            var options = new DbContextOptionsBuilder<EntityFrameworkRepository>()
                .UseSqlServer(config.ConnectionString)
                .Options;

            using (var repo = new EntityFrameworkRepository(options))
            {
                repo.Initialize();

                using (var transaction = repo.Database.BeginTransaction())
                {
                    var entity = new Entity { Content = "Any test Content", Type = "TestType1" };

                    repo.Add(entity);
                    repo.SaveChanges();

                    var dataService = new DataService(Options.Create(config), repo);

                    var resultEntities = dataService.GetEntities(entity.Type);

                    Assert.IsTrue(resultEntities.Any());
                    Assert.AreSame(resultEntities.First().Content, entity.Content);
                    Assert.AreSame(resultEntities.First().Type, entity.Type);
                }
            }
        }        
    }
}
