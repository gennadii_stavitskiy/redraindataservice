﻿using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace RedRainData.Tests.Repository
{
    public static class DbSetExtensions
    {
        public static DbSet<T> MockFromSql<T>(this DbSet<T> dbSet, SpAsyncEnumerableQueryable<T> spItems) where T : class
        {
            var queryProviderMock = new Mock<IQueryProvider>();
            queryProviderMock.Setup(p => p.CreateQuery<T>(It.IsAny<MethodCallExpression>()))
                .Returns<MethodCallExpression>(x =>
                {
                    return spItems;
                });

            var dbSetMock = new Mock<DbSet<T>>();
            dbSetMock.As<IQueryable<T>>()
                .SetupGet(q => q.Provider)
                .Returns(() =>
                {
                    return queryProviderMock.Object;
                });

            dbSetMock.As<IQueryable<T>>()
                .Setup(q => q.Expression)
                .Returns(Expression.Constant(dbSetMock.Object));
            return dbSetMock.Object;
        }
    }

    public class SpAsyncEnumerableQueryable<T> : IAsyncEnumerable<T>, IQueryable<T>
    {
        private IAsyncEnumerable<T> _spItems;
        public Expression Expression => _spItems.ToEnumerable().AsQueryable().Expression;
        public Type ElementType => typeof(T);
        public IQueryProvider Provider => _spItems.ToEnumerable().AsQueryable().Provider;

        public SpAsyncEnumerableQueryable(params T[] spItems)
        {
            _spItems = AsyncEnumerable.ToAsyncEnumerable(spItems);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _spItems.ToEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetEnumerator()
        {
            return _spItems.GetEnumerator();
        }
    }
}
