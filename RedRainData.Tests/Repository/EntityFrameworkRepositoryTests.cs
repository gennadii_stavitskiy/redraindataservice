using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RedRainData.Model;
using RedRainData.Repository;
using System;
using System.Linq;

namespace RedRainData.Tests.Repository
{
    [TestClass]
    public class EntityFrameworkRepositoryTests
    {
        DbContextOptions<EntityFrameworkRepository> _options;

        [TestInitialize]
        public void TestInitialize()
        {
            // UseInMemoryDatabase
            _options = new DbContextOptionsBuilder<EntityFrameworkRepository>()
                .UseInMemoryDatabase(databaseName: "GetByType results")
                .Options;
        }
        
        [TestMethod]
        public void EntityFrameworkRepository_CanCreate()
        {            
            using (var repo = new EntityFrameworkRepository(_options)) { };
        }
                
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void EntityFrameworkRepository_GetEntities_ThrowArgumentNullExceptionIfParameterIsNull()
        {
            using (var repo = new EntityFrameworkRepository(_options))
            {
                repo.GetByType(null);
            };
        }

        [TestMethod]
        public void EntityFrameworkRepository_Add_EntityAddedToDB()
        {     
            var entity = new Entity { Type = "T1", Content = "Any Content" };

            using(var repo = new EntityFrameworkRepository(_options))
            {
                repo.Add(entity);
                repo.SaveChanges();
            }

            using (var repo = new EntityFrameworkRepository(_options))
            {
                Assert.AreEqual(1, repo.Entities.Count());
                Assert.AreEqual("Any Content", repo.Entities.Single().Content);
            }
        }

        [TestMethod]
        public void EntityFrameworkRepository_GetEntities_ReturnsCorrectEntity()
        {
            var entity1 = new Entity { Type = "T1", Content = "Any Content1" };
            var entity2 = new Entity { Type = "T1", Content = "Any Content2" };

            var entities = new SpAsyncEnumerableQueryable<Entity>( entity1, entity1 );
            
            var entityFrameWorkRepo = new EntityFrameworkRepository(_options);

            entityFrameWorkRepo.Entities = entityFrameWorkRepo.Entities.MockFromSql(entities);

            var resultEntities =  entityFrameWorkRepo.GetByType("T1");

            Assert.IsTrue(resultEntities.Count == 2);
        }
    }
}
