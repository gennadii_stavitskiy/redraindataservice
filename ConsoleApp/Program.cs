﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RedRainData;
using RedRainData.Model;
using RedRainData.Repository;
using System;
using System.Collections.Generic;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //the first approach to integrate repo
            var config = ConfigurationSettings.Default();

            var options = new DbContextOptionsBuilder<EntityFrameworkRepository>()
               .UseSqlServer(config.ConnectionString)
               .Options;

            using (var repo = new EntityFrameworkRepository(options))
            {
                repo.Initialize();                

                var result1 = (new DataService(Options.Create(config), repo)).GetEntities("t1");

                Console.WriteLine("Result1");
                WriteToConsole(result1);
            }

            //same result
            using(var dataService = new DataService())
            {
                var result2 = dataService.GetEntities("t1");

                Console.WriteLine("Result2 Must be equal with Result1");
                WriteToConsole(result2);

            }

            Console.ReadKey();
        }

        private static void WriteToConsole(List<Entity> results)
        {
            foreach (var r in results)
            {
                Console.WriteLine($"id:{r.Id}, Type:{r.Type}, Content:'{r.Content}', Created:{r.Created}");
            }
        }
    }
}
